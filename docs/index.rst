O le Nintendo Switch Ua lauiloa i le atinaʻeina e lona igoa igoa NX, o le manatu o le Suiga na amata mai a Nintendo tali atu i le tele o kuata o tupe leiloa i le 2014, mafua mai i le le lelei faʻatau o lana muamua console, le Wii U, ma maketi tauvaga mai feaveaʻi taʻaloga. O le sui peresetene o Nintendo, Satoru Iwata, na unaʻi le kamupani agai i taʻaloga feaveaʻi ma mea faigaluega fou.

O le Nintendo Switch's design e faʻatatau i se lautele lautele o tagata taʻaalo vitio e ala i le tele o auala e faʻaaoga ai. Nintendo filifili e faʻaaoga sili atu elemeni elemeni, pei o se chipset faʻavae i luga o le Nvidia's Tegra laina, ia faia atinaʻe mo le faʻamafanafana faigofie mo polokalame ma sili atu fetaui ma masini taʻaloga o loʻo iai.

A o tauivi le Wii U e maua le lagolago mai fafo, tuua ai ma le vaivai polokalama faletusi, Nintendo muamua saili le lagolago a le tele o isi-vaega atinaʻe ma tagata lolomitusi e fesoasoani fausiaina le Switch's game faletusi faʻatasi ai ma Nintendo uluaʻi pati pati, aofia ai le tele tutoʻatasi potu ata vitio. A o Nintendo muamua fuafuaina le tusa o le 100 ulutala mo lona tausaga muamua, i luga o 320 ulutala mai muamua-pati, lona tolu-pati, ma tutoatasi atinae na faʻasaʻolotoina i le faaiuga o le 2017.

# Heading level 1
